<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TasksController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $todos=\App\Todo::all();
        return view('index',compact('todos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $todo= new \App\Todo;
        $todo->title=$request->get('title');
        $todo->description=$request->get('description');
        $checked = $request->has('done') ? $todo->done=true : $todo->done=false;
        $todo->save();
        
        return redirect('task')->with('success', 'Information has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //iria fazer uma pagina pra mostrar somente os afazeres sem estar no painel mas to com sono
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $todos=\App\Todo::find($id);
        return view('edit',compact('todos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $todo= \App\Todo::find($id);
        $todo->title=$request->get('title');
        $todo->description=$request->get('description');
        $todo->done= $request->get('done') == 'on' ? $todo->done=1 : $todo->done=0;
        $todo->save();
        return redirect('task')->with('success', 'Information has been modified');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $todo = \App\Todo::find($id);
        $todo->delete();
        return redirect('/task')->with('success','Information has been deleted');
    }
}
