<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
    <!-- Styles -->
    <style>
        html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
</head>

<body>
    <div class="flex-center position-ref full-height">


        <div class="content">
            <div class="title m-b-md">
                CREATE
            </div>


                <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
        <form method="post" action="{{url('task')}}" enctype="multipart/form-data">
        @csrf
				<div class="form-group">
					 
					<label for="title">
						Title
					</label>
					<input type="text" class="form-control" id="title" name="title" />
				</div>
				<div class="form-group">
					 
					<label for="description">
						Description
					</label>
					<input type="text" class="form-control" id="description" name="description"/>
				</div>
				
				<div class="checkbox">
					 
					<label>
						<input type="checkbox" id="done" name="done"/> done
					</label>
				</div> 
				<button type="submit" class="btn btn-primary">
					Create
				</button>
			</form>
		</div>
	</div>
</div>
        </div>
</body>

</html>