<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
    <!-- Styles -->
    <style>
        html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
</head>

<body>

    <div class="flex-center position-ref full-height">

    
        <div class="content ">

            <div class="title m-b-md">
                TODO
            </div>

@if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
                <table id="mytable" class="table table-bordred table-striped">

                    <thead>
                    <th></th>
                        <th>Titulo</th>
                        <th>Descrição</th>
                        <th>Done</th>
                        <th>Edit</th>
                        <th>Delete</th>
                        <th><a href="{{action('TasksController@create')}}" class="btn btn-xs btn-success"><span class="glyphicon glyphicon-plus"></span> New</a></th>
                    </thead>
                    <tbody>
                    @foreach($todos as $todo)
                        @php
                            @endphp
                        <tr>
                            <th class="text-nowrap" scope="row"><input type="checkbox" class="checkthis" /></th>
                            <td>{{$todo['title']}}</td>
                            <td>{{$todo['description']}}</td>
                            <td>{{$todo['done'] == 0 ? "no" : "yes"}} </td>
                            
                            <td>
                                <a href="{{route('task.edit', $todo['id'])}}"class="btn btn-primary btn-xs"
                                        data-title="Edit" data-toggle="modal" data-target="#edit"><span class="glyphicon glyphicon-pencil"></span></a></p>
                            </td>
                            <td>
                                <form action="{{route('task.destroy', $todo['id'])}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                                </form>
                            </td>
                            <td>
                                <p data-placement="top" data-toggle="tooltip" title="Delete"><a href="" 
                                        data-title="Delete" data-toggle="modal" data-target="#delete"></a></p>
                            </td>
                            <td></td>
                        </tr>
                        @endforeach
                        <tr>
                        @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
        </div>
</body>

</html>